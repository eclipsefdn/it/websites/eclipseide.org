/*!
 * Copyright (c) 2021, 2022 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 *
 * SPDX-License-Identifier: EPL-2.0
 */

import { renderVideos } from 'eclipsefdn-solstice-assets/js/privacy/eclipsefdn.videos';
import additionalInfoTemplate from '../templates/additional-info.mustache';

(async function displayNewAndNoteworthy() {
  if (!window.location.pathname.includes('noteworthy')) {
    return;
  }

  // Get all releases data
  const allReleases = await (await fetch('https://projects.eclipse.org/api/simultaneous_release/')).json();
  const releaseNameArray = await (await fetch('/data/release-name.json')).json();

  // Convert the response data from object to array
  // Filter out jakarta release and releases older than 2018-12 (1537329600)
  // Ordered by release date, newest first.
  const dataArray = Object.entries(allReleases)
    .map(([key, value]) => ({ release_name: key, ...value }))
    .filter((release) => !release.release_name.includes('jakarta') && release.release_date > '1537329600' && releaseNameArray.find(element => element.date === release.release_name))
    .sort((a, b) => b.release_date - a.release_date);

  const [latestRelease, ...previousReleases] = dataArray;

  const showSelectedNN = (selectedName, container) => {
    const selectedRelease = dataArray.find((release) => release.release_name === selectedName);
    const projectsArray = Object.values(selectedRelease.projects);
    // Convert Unix time to human readable date string
    const dateString = new Date(parseInt(selectedRelease.release_date) * 1000).toDateString();
    const quarterText = () => {
      // Get the month from release name "yyyy-mm"
      switch (selectedName.substring(5, 7)) {
        case '03':
          return 'first';
        case '06':
          return 'second';
        case '09':
          return 'third';
        case '12':
          return 'fourth';
        default:
          break;
      }
    };

    const releaseObj = releaseNameArray.find((item) => item.date === selectedName);
    const newsURL = 'https://www.eclipse.org/eclipse/news/';
    const descriptionHTML = `<p>The ${selectedName} release is the Eclipse Foundation's ${quarterText()}
    quarterly simultaneous release in ${selectedName.substring(0, 4)} with
    <a href="https://projects.eclipse.org/releases/${selectedName}">${projectsArray.length} participating projects</a>,
    available ${dateString.substring(4, 10)}, ${dateString.substring(11, 15)}.</p>
    <p>The Eclipse SDK project is part of the Eclipse ${selectedName} simultaneous release. The Eclipse SDK and related resources can be downloaded from the <a href="https://download.eclipse.org/eclipse/downloads/">Eclipse Project downloads page</a>. The release notes for it can be found <a href="https://www.eclipse.org/eclipse/development/readme_eclipse_${
      releaseObj.version
    }.php">here</a>.</p>
    <p>Here are the new and noteworthy items of the Eclipse Platform in this release:</p>
    <ul>
      <li><a href="${newsURL}${releaseObj.version}/platform.php">New features in the Platform and Equinox</a></li>
      <li><a href="${newsURL}${releaseObj.version}/jdt.php">New features for Java developers</a></li>
      <li><a href="${newsURL}${releaseObj.version}/platform_isv.php">New APIs in the Platform and Equinox</a></li>
      <li><a href="${newsURL}${releaseObj.version}/pde.php">New features for plug-in developers</a></li>
    </ul>
    <p>Here are the new and noteworthy items of various projects in the release train:</p>`;

    const ul = document.createElement('ul');
    // Generate new and noteworthy list
    projectsArray
      .filter((item) => item.new_and_noteworthy_url)
      .forEach((item) => {
        const li = document.createElement('li');
        const a = document.createElement('a');
        a.href = item.new_and_noteworthy_url;
        a.textContent = item.project_name;
        li.appendChild(a);
        ul.appendChild(li);
      });

    container.innerHTML = descriptionHTML;
    container.appendChild(ul);
    if (releaseObj.version.startsWith('4.') && parseFloat(releaseObj.version.substring(2)) < 31) { // Since release 4.31 Eclipse-Platform has a Windows-Defender auto-fix built in 
      container.innerHTML = container.innerHTML + additionalInfoTemplate();
    }
    if (releaseObj.videos?.length > 0) {
      container.innerHTML += `
        <h2>Highlights of ${selectedName}</h2>
        <div class="row">
          ${releaseObj.videos.map((video) => `
            <div class="col-sm-10 margin-bottom-30">
              <a class="eclipsefdn-video" href="${video}"></a>
            </div>
          `)}
        </div>
      `;
    }

    renderVideos();
  };

  const latestNoteworthyContainer = document.querySelector('.eclipsefdn-latest-noteworthy');
  showSelectedNN(latestRelease.release_name, latestNoteworthyContainer);

  // For previous new and noteworthy dropdown menu
  const dropdown = document.querySelector('#dropdown-menu-nn');
  dropdown.innerHTML = '';
  previousReleases.forEach((release) => {
    const li = document.createElement('li');
    // Create a link for each release. This will allow the list to be tabbed
    // through and selected with the keyboard.
    const a = document.createElement('a');
    a.href = '#pre-noteworthy';
  
    const preNoteworthyContainer = document.querySelector('.eclipsefdn-pre-noteworthy');
    const releaseName = release.release_name;
    a.textContent = releaseName;

    a.addEventListener('click', () => showSelectedNN(releaseName, preNoteworthyContainer));
    li.appendChild(a);
    dropdown.appendChild(li);
  });

  document.querySelector('.loading-container').innerHTML = '';
  document.querySelector('.dropdown-new-noteworthy').classList.remove('hide');
})();
