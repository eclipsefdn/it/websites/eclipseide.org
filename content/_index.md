---
title: "Eclipse IDE"
seo_title: "Eclipse IDE"
keywords: ["download", "features", "latest", "ide", "java", "eclipse"]
content_classes: "padding-top-50"
header_wrapper_class: "header-eclipseide"
date: 2022-08-15
hide_sidebar: true
hide_page_title: true
hide_breadcrumb: true
show_featured_story: true
show_featured_footer: false
headline: ""
custom_jumbotron: |
  <h1 class="event-title uppercase">Eclipse IDE</h1>
  <p class="event-subtitle">The Leading Open Platform for <br> Professional Developers</p>
  <div class="release-btns">
    <a class="download-btn btn btn-primary btn-block margin-bottom-20" href="https://www.eclipse.org/downloads/packages" target="_blank">Download 2024-12</a>
    <div class="btn-row">
      <a class="btn btn-default" href="https://www.eclipse.org/downloads/packages/release" target="_blank">Other Releases</a>
      <a class="btn btn-default" href="https://github.com/eclipse-ide" target="_blank">Contribute</a>
      <a class="btn btn-default" href="https://www.eclipse.org/sponsor" target="_blank">Sponsor</a>
    </div>
  </div>
container: "container-fluid eclipse-ide-release"
layout: "single"
page_css_file: "/public/css/release.css"
---

{{< pages/home/new-features >}}
{{< pages/home/featured-stats >}}
{{< pages/home/featured-videos >}}
{{< testimonials_carousel class="margin-bottom-10" >}}
{{< pages/home/featured-footer >}}
